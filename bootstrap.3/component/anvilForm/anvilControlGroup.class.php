<?php
require_once PHPANVIL2_COMPONENT_PATH . 'anvilContainer.class.php';


/**
 * Fieldset Control
 *
 * @copyright     Copyright (c) 2012 Nick Slevkoff (http://www.slevkoff.com)
 */
class anvilControlGroup extends anvilContainer
{

    public $label;
    public $labelForID;
//	public $controlsClass = 'col-xs-12 col-sm-9 col-md-7 col-lg-5';
//	public $labelClass = 'col-sm-3 col-md-3 col-lg-2';

	public $controlsClass = 'col-xs-9';

	public $labelClass    = 'col-xs-3';


	public function __construct($id = '', $label = '', $labelForID = '', $properties = null)
    {

        $this->enableLog();

        parent::__construct($id, $properties);

        $this->label = $label;
        $this->labelForID = $labelForID;
    }


    public function renderContent()
    {

        $return = '<div class="form-group';
        if (!empty($this->class)) {
            $return .= ' ' . $this->class;
        }
        $return .= '">';

        if (!empty($this->label)) {
            $return .= '<label class="control-label';
	        if (!empty($this->labelClass)) {
		        $return .= ' ' . $this->labelClass;
	        }
	        $return .= '" for="' . $this->labelForID . '">' . $this->label . '</label>';
//	        $return .= '<label for="' . $this->labelForID . '">' . $this->label . '</label>';
        }

        $return .= '<div class="controls';
	    if (!empty($this->controlsClass)) {
		    $return .= ' ' . $this->controlsClass;
	    }

	    $return .= '">';

        $return .= $this->renderControls();

        $return .= '</div>';
        $return .= '</div>';


        return $return;
    }

}

<?php
require_once PHPANVIL2_COMPONENT_PATH . 'anvilContainer.class.php';


/**
 * Fieldset Control
 *
 * @copyright     Copyright (c) 2012 Nick Slevkoff (http://www.slevkoff.com)
 */
class anvilFieldset extends anvilContainer
{

    public $title;
    public $actions;
    public $titleActions;
//	public $formActionsClass = 'col-sm-offset-3 col-sm-9 col-md-offset-3 col-md-7 col-lg-offset-2 col-lg-5';

	public $formActionsClass = 'col-xs-offset-3 col-xs-9';

	public $tag = 'fieldset';
	public $titleClass;
	public $titleTag = 'legend';


    public function __construct($id = '', $title = '', $properties = null)
    {

        $this->enableLog();

        parent::__construct($id, $properties);

        $this->title = $title;
        $this->titleActions = new anvilContainer();

        $this->actions = new anvilContainer();
    }


    public function renderContent()
    {

        $return = '<' . $this->tag;

        if (!empty($this->class)) {
            $return .= ' class="' . $this->class . '"';
        }

        if ($this->style) {
            $return .= ' style="' . $this->style . '"';
        }

        $return .= '>';

        if (!empty($this->title)) {
            //---- TODO: Revert back to <legend> tag once Chrome and Safari
            //---- fix their browsers not rendering margins on legend tags
            //---- within fieldsets.
            $return .= '<' . $this->titleTag;

	        if (!empty($this->titleClass)) {
		        $return .= ' class="' . $this->titleClass . '"';
	        }

	        $return .= '>' . $this->title;
//            $return .= '<div class="legend">' . $this->title;
        }

        $titleActions = $this->titleActions->renderControls();
        if (!empty($titleActions)) {
            $return .= '<div class="actions">';
            $return .= $titleActions;
            $return .= '</div>';
        }

        if (!empty($this->title)) {
            $return .= '</' . $this->titleTag . '>';
//            $return .= '</div>';
        }

        $return .= $this->renderControls();

        $actions = $this->actions->renderControls();

        if (!empty($actions)) {
            $return .= '<div class="form-actions';
	        if (!empty($this->formActionsClass)) {
		        $return .= ' ' . $this->formActionsClass;
	        }
	        $return .= '">';
            $return .= $actions;
            $return .= '</div>';
        }

        $return .= '</' . $this->tag . '>';


        return $return;
    }

}

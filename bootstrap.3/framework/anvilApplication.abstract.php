<?php
require_once(PHPANVIL2_COMPONENT_PATH . 'anvilObject.abstract.php');


abstract class anvilApplicationAbstract extends anvilObjectAbstract
{

	public $account;

	public $build              = '1';

	public $catchAllAction;

	public $catchAllModule;

	public $configFilename     = 'application.config.php';

	public $cookieAccountToken = '_anvila';

	public $cookieUserID       = '_anvilu';

	public $cookieUserToken    = '_anvilt';

	public $copyright          = '(c) 2012';

	public $copyrightHTML      = '&copy; 2012';

	/** @var phpAnvil2 */
	public $core;

	public $cryptKey      = 'anvil';

	public $defaultAction;

	public $defaultModule = 'phpAnvil';

	public $defaultURL;

	public $forceSSL      = false;

	public $isWAS         = false;

	public $loginAction;

	public $loginModule;

	public $loginURL;

	public $name          = 'New Application';

//	/** @var  \kodevo\kore\Application\ApplicationInterface $portalApp */
//	public $portalApp;

	public $refName = 'App';

	//---- Application Encryption Key - OVERRIDE PER APPLICATION ---------------

	public $requestedAction;

	public $requestedModule = 'phpAnvil';

	/**
	 * @var anvilUserModelAbstract
	 */
	public    $user;

	public    $version    = '0.1';

	protected $meta       = array();

	protected $production = true;


	function __construct()
	{
		global $phpAnvil;

		$this->core = $phpAnvil;

		$headers = apache_request_headers();

		$this->isWAS = array_key_exists(strtolower('x-was'), array_change_key_case($headers));
	}


	function authenticateUser()
	{
		global $phpAnvil;

		$phpAnvil->triggerEvent('application.authenticateUser');

		$return = $phpAnvil->userAuthenticated;

		return $return;
	}


	function close()
	{
		global $phpAnvil;

		//---- Check if Site is Set
		if (isset($phpAnvil->site)) {
			//---- Initialize the Site
			$phpAnvil->site->close();
			$return = true;
		} else {
			$this->_logError('Site not set in phpAnvil.');
		}

		$phpAnvil->triggerEvent('application.close');

		return $return;
	}


	public function getMeta($name)
	{
		if (isset($this->meta[$name])) {
			return $this->meta[$name];
		} else {
			return null;
		}
	}


	public function setMeta($name, $value)
	{
		$this->meta[$name] = $value;
	}


	function init()
	{
//        global $phpAnvil;

		$return = false;

		$this->loadConfig();

//        $this->core->triggerEvent('application.init');


		//---- Check if Site is Set
		if (isset($this->core->site)) {
			//---- Initialize the Site
			$this->core->site->init();
			$return = true;

			$this->defaultURL = $this->core->site->webPath;
			$this->loginURL = $this->defaultURL . 'Login/';

		} else {
			$this->_logError('Site not set in phpAnvil.');
//            FB::error('Site not set in phpAnvil.');
		}

		return $return;
	}


	public function initDebug()
	{

	}


	public function isFeatureEnabled($name)
	{
		return false;
	}


	/**
	 * @return boolean
	 */
	public function isProduction()
	{
		return $this->production;
	}


	public function isVendorEnabled($name)
	{
		return false;
	}


	function loadConfig()
	{
		global $phpAnvil;

		$return = false;

		$filePath = APP_PATH . $this->configFilename;
		if (file_exists($filePath)) {
			include_once $filePath;

//            $this->_logVerbose('Application config file, ' . $this->configFilename . ', loaded.');

			$return = true;
		}
	}


	function open()
	{
//        global $phpAnvil;

		$return = false;

//        $phpAnvil->triggerEvent('application.open');


		//---- Check if Site is Set
		if (isset($this->core->site)) {
			//---- Initialize the Site
			$this->core->site->open();
			$return = true;
		} else {
//            FB::error('Site not set in phpAnvil.');
			$this->_logError('Site not set in phpAnvil.');
		}

		return $return;
	}


	public function renderSid()
	{
		return bin2hex(openssl_random_pseudo_bytes(2)) . $this->meta['node_sid'] .
			dechex(round(microtime(true) - 1400000000, 6) * 1000000);
	}
}

<?php
/*
 * phpAnvil Framework
 *
 * Copyright (c) 2009-2011 Nick Slevkoff
 *
 * LICENSE
 *
 * This source file is subject to the MIT License that is bundled with this
 * package in the file LICENSE.md.  It is also available online at the following:
 * - http://www.phpanvil.com/LICENSE.md
 * - http://www.opensource.org/licenses/mit-license.php
 */

//namespace phpAnvil\Framework;

require_once dirname(__FILE__) . '/../component/ObjectAbstract.php';
//require_once dirname(__FILE__) . '/../component/ComponentAbstract.php';
//require_once 'kernel.php';
//require_once 'autoclassloader.php';

//use phpAnvil\Framework;

//---- phpAnvil Primary Class ----------------------------------------------
//$phpAnvil = new kernel();


//---- Auto Class Loader ---------------------------------------------------
//$phpAnvil->autoClassLoader = new AutoClassLoader();
//$phpAnvil->autoClassLoader->addNamespace('phpAnvil', realpath(dirname(__FILE__) . '/..'));
//$phpAnvil->autoClassLoader->open();

